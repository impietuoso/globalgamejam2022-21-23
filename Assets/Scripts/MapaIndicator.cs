using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapaIndicator : MonoBehaviour
{
    public static MapaIndicator instance;
    public GameObject[] indicators;
    int count = 0;
    public GameObject map;
    private void Awake() {
        instance = this;
    }
    public void ActiveMap() {
        indicators[count].SetActive(true);
        count++;
        if(count == indicators.Length) {
            Invoke("AlphaClose", 1f);
        }
    }

    public void AlphaClose() {
        StartCoroutine(HideMap());
        Debug.Log("Chamou Corrotine");
    }
    IEnumerator HideMap() {
        Debug.Log("Corrotina iniciada");
        //map.GetComponent<CanvasGroup>().alpha = Mathf.MoveTowards(1f, 0f, Time.deltaTime/3);
        while (map.GetComponent<CanvasGroup>().alpha > 0) {
            map.GetComponent<CanvasGroup>().alpha -= Time.deltaTime;
            //map.GetComponent<CanvasGroup>().alpha = Mathf.MoveTowards(1f, 0f, 5f);
            yield return null;
            Debug.Log("Cada frame chamado");
        }
    }
}
