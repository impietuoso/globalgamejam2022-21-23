using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionSample : MonoBehaviour,IInteractable {
    
    public void Interact(){
        PlayerController.instance.TogglePlayers();
    }

}
