using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameBandAid : MonoBehaviour {
    
    public static EndGameBandAid instanace{get;protected set;}

    public float FadeTime,StandByTime;

    public UnityEngine.Events.UnityEvent OnStandByEnd;
    CanvasGroup _group;

    void Awake(){
        instanace = this;
        _group = GetComponent<CanvasGroup>();
    }

    public void ShowMessage(){
        LeanTween.value(gameObject,(val)=>_group.alpha = val,
                        0,1,FadeTime).setOnComplete(()=>StartCoroutine(GetOutTimer()));
    }

    IEnumerator GetOutTimer(){
        yield return new WaitForSecondsRealtime(StandByTime);
        OnStandByEnd?.Invoke();
    }

}
