using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSample : MonoBehaviour,IInteractable {
    
    [SerializeField]AudioClip AudioThingy;
    [SerializeField]RectTransform TheThing;
    [SerializeField] UnityEngine.Events.UnityEvent AfterTheThing;

    AudioSource _source;

    bool _played;

    void Awake() {
        _source = GetComponent<AudioSource>();
    }
    public void DoTheThing(){
        if(_played)
            return;
        TheThing.localScale = new Vector2(.5f,.5f);
        TheThing.GetComponent<UnityEngine.UI.Image>().color = new Color(1,1,1,0);
        TheThing.gameObject.SetActive(true);
        _source.PlayOneShot(AudioThingy);
        StartCoroutine(DoingTheThing());
    }

    IEnumerator DoingTheThing(){
        //float tempVal = AudioManager.instance.musicAS.volume;
        PlayerController.instance.enabled = false;
        var thing = TheThing.GetComponent<UnityEngine.UI.Image>();
        float a = 0;
        while(_source.isPlaying){
            yield return new WaitForEndOfFrame();
            a += .001f;
            TheThing.localScale += new Vector3(.001f,.001f);
            thing.color = new Color(1,1,1,a);
        }
        TheThing.gameObject.SetActive(false);
        AfterTheThing?.Invoke();
        PlayerController.instance.enabled = true;
        Destroy(gameObject,.1f);
        _played = true;
        //AudioManager.instance.musicAS.volume = tempVal;
    }

    public void Interact(){
        DoTheThing();
    }

}
