using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

public class ControllerDebug : MonoBehaviour {
    
    GameInputs _control;

    void Awake(){
        _control = new GameInputs();
    }

    void OnEnable(){
        InputUser.listenForUnpairedDeviceActivity = 1;
        _control.Enable();
        InputUser.onUnpairedDeviceUsed += onUnpairedDeviceUsed;
    }

    private void OnDisable() {
        InputUser.listenForUnpairedDeviceActivity = 0;
        _control.Disable();
        InputUser.onUnpairedDeviceUsed -= onUnpairedDeviceUsed;
    }

    void onUnpairedDeviceUsed(InputControl control,UnityEngine.InputSystem.LowLevel.InputEventPtr ptr){
        print(control.device.ToString());
        
        //print(_control.GamepadScheme.SupportsDevice(control.device));
    }

}
