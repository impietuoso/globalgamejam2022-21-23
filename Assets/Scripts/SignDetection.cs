using UnityEngine;

public class SignDetection : MonoBehaviour
{
    bool hasSomeOneInside;
    public GameObject Tutorial;

    void OnTriggerEnter2D(Collider2D col) {
        if (col.tag == "Player" && !hasSomeOneInside) {
            hasSomeOneInside = true;
            Tutorial.SetActive(true);
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.tag == "Player" && hasSomeOneInside) {
            hasSomeOneInside = false;
            Tutorial.SetActive(false);
        }
    }
}
