using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimEvents : MonoBehaviour {
    
    public void PlayAudio(sfxID id){
        AudioManager.instance.PlaySound(id);
    }

}
