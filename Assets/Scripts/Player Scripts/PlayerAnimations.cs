using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour {
    
    Animator anim;
    public string curState { get; protected set; }

    Dictionary<string,float> _animationTimer;

    void Awake() {
        _animationTimer = new Dictionary<string, float>();
        anim = GetComponent<Animator>();
        curState = anim.GetCurrentAnimatorClipInfo(0)[0].clip.name;
    }

    public void ChangeState(string state) {
        if(state == curState) return;
        anim.Play(state,0);
        curState = state;
    }

    public float ChangeStateTimed(string state){
        if(state == curState) return 0;
        anim.Play(state,0);
        curState = state;
        return _animationTimer[anim.GetCurrentAnimatorClipInfo(0)[0].clip.name];
    }

}
