using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using UnityEngine.Experimental.Rendering.LWRP;

// KIRYYYYYYYYYYYYYYYYYU-CHAAAAAAAAAAAAAAAAAN
public class PlayerController : MonoBehaviour {
    
    public static PlayerController instance {get; protected set;}

    // for readability, but i'm not sure if i should be doin' it
    public static Vector2 WallJumpForce => instance.wallJumpForce;
    public static float WallSlideSpeed => instance.wallSlideSpeed;
    public static float DashDuration => instance.dashDuration;
    public static float DashSpeed => instance.dashSpeed;

    [SerializeField]GameObject PauseMenu;
    [SerializeField]float MoveSpeed,JumpForce,dashDuration,dashSpeed,wallSlideSpeed;
    [SerializeField]Vector2 wallJumpForce;
    [SerializeField]PlayerMovement[] PlayerCharactes;
    [SerializeField]UnityEngine.Events.UnityEvent OnToggleBegin,OnToggleEnd,ExternalCall;
    [SerializeField,Tooltip("-1 = para o personagem e espera uma chamada externa")]
    float ToggleTimer;
    [SerializeField]BoolEvent IsUsingGamepad;
    GameInputs _inputs;
    PlayerMovement _targetPlayer;
    bool _lockInputs;

    [SerializeField]
    private UnityEngine.Experimental.Rendering.Universal.Light2D globalLight;
    [SerializeField]
    private UnityEngine.Experimental.Rendering.Universal.Light2D pointLight;

    void Awake() {
        instance = this;
        _targetPlayer = PlayerCharactes[0];
        SetInputCallbacks();
        if(pointLight != null)
            pointLight.intensity = 0;
        if(globalLight != null)
            globalLight.intensity = 1;
    }

    void OnEnable(){
        InputUser.listenForUnpairedDeviceActivity = 1;
        _inputs.Enable();
        InputUser.onUnpairedDeviceUsed += onUnpairedDeviceUsed;
    }

    void OnDisable(){
        InputUser.listenForUnpairedDeviceActivity = 0;
        _inputs.Disable();
        InputUser.onUnpairedDeviceUsed -= onUnpairedDeviceUsed;
    }

    void onUnpairedDeviceUsed(InputControl control,UnityEngine.InputSystem.LowLevel.InputEventPtr ptr){
        //print(control.device);
        if(_inputs != null)
            IsUsingGamepad?.Invoke(_inputs.GamepadScheme.SupportsDevice(control.device));
    }

    void SetInputCallbacks(){
        _inputs = new GameInputs();
        _inputs.GameActions.Movement.performed += (ctx) => Move(ctx.ReadValue<float>());
        _inputs.GameActions.Movement.canceled += (ctx) => Move(0);
        _inputs.GameActions.Jump.performed += (ctx) => Jump();
        _inputs.GameActions.Intreraction.performed += (ctx) => Interact();
        _inputs.GameActions.Special.performed += (ctx) => Special();
        _inputs.GameActions.Alternate.performed += (ctx) => RequestToggle();
        _inputs.GameActions.Menu.performed += (ctx) => Pause();
    }

    void Move(float val){
        if(!_lockInputs)
            _targetPlayer.Move(val,MoveSpeed);
    }

    void Jump(){
        if(!_lockInputs)
            _targetPlayer.Jump(JumpForce);
    }

    void Interact(){
        if(!_lockInputs)
            _targetPlayer.Interact();
    }

    void Special(){
        if(!_lockInputs)
            _targetPlayer.Special();
    }

    void RequestToggle(){
        if(!_lockInputs){
            _targetPlayer.ForceStop();
            _lockInputs = true;
            OnToggleBegin?.Invoke();
            if(ToggleTimer > 0)
                StartCoroutine(BeginToggleTimer());
            else
                ExternalCall?.Invoke();
        }
    }

    public void Pause(){
        PauseMenu.SetActive(!PauseMenu.activeSelf);
        Time.timeScale = Time.timeScale > 0 ? 0 : 1;
        enabled = !enabled;
        AudioManager.instance.PlaySound(enabled ? 7 : 5);
    }
    
    public void TogglePlayers(){
        _targetPlayer = _targetPlayer == PlayerCharactes[0] ? PlayerCharactes[1] : PlayerCharactes[0];
        _lockInputs = false;
        OnToggleEnd?.Invoke();
        //return;
        if (PlayerCharactes[0]) { 
            globalLight.intensity = 1;
            pointLight.intensity = 0;
        }

        if (PlayerCharactes[1]) { 
            globalLight.intensity = 0.6f;
            pointLight.intensity = 0.8f;
        }
    }

    IEnumerator BeginToggleTimer(){
        AudioManager.instance.PlaySound(sfxID.changeCharacters);
        ParticleManager.instance.PlayActiveParticle((_targetPlayer == PlayerCharactes[0] ? PlayerCharactes[1] : PlayerCharactes[0]).gameObject);
        ParticleManager.instance.PlayDeactiveParticle(_targetPlayer.gameObject);
        yield return new WaitForSeconds(ToggleTimer);
        TogglePlayers();
    }

}
