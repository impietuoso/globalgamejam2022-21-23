using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    
    public sfxID GoundSlamSfx;
    enum PlayerState { Ground,Air,Frozen }

    [System.Flags]public enum SpecialMove : byte {
         DoubleJump = 0b_0000_0001, 
         WallJump = 0b_0000_0010, 
         Dash = 0b_0000_0100 
    }

    float WallSlideSpeed => PlayerController.WallSlideSpeed;

    [SerializeField]Transform WallCheckObj;
    [SerializeField]ParticleSystem WallSlideParticle;
    [SerializeField]SpecialMove specialMove;
    [SerializeField]bool OverrideValues;
    [SerializeField]float MoveSpeed;
    [SerializeField]float DashGhosts;


    PlayerState _playerState;
    Rigidbody2D _rb2d;
    float _spd;
    float _inputDowntime,_dashDuration;

    IInteractable _interactable;

    bool _usedDoubleJump,_touchingWall,_isWallSliding,_airDashed,_canWallJump;

    PlayerAnimations _anims;

    CapsuleCollider2D _collider; 

    void Awake() {
        _rb2d = GetComponent<Rigidbody2D>();
        _anims = GetComponent<PlayerAnimations>();
        _collider = GetComponent<CapsuleCollider2D>();
    }

    void Update() {
        if(_playerState == PlayerState.Frozen)
            return;
        GroundCheck();
        if((specialMove & SpecialMove.WallJump) != 0)
            WallCheck();
        if(_inputDowntime > 0){
            if(Mathf.Abs(_rb2d.velocity.x) > .05f)
                transform.localScale = new Vector2(_rb2d.velocity.x > 0 ? 1 : -1,1);
        } else if(_spd != 0)
            transform.localScale = new Vector2(_spd > 0 ? 1 : -1,1);
        UpdateAnimations();
    }

    void FixedUpdate() {
        if(_inputDowntime <= 0)
            _rb2d.velocity = new Vector2(_spd,_isWallSliding ? -WallSlideSpeed : _rb2d.velocity.y);
        else
            _inputDowntime -= Time.fixedDeltaTime;
    }

    void UpdateAnimations(){
        if(_inputDowntime > 0)
            return;
        if(_playerState == PlayerState.Air){
            if(_canWallJump){
                _anims.ChangeState("WallSlide");
                return;
            }
            _anims.ChangeState("Jump");
            return;
        }
        _anims.ChangeState(_spd != 0 ? "Walk" : "Idle");
    }

    private void OnDrawGizmos() {
        if(!Application.isPlaying)
            return;
        float boxOffset = .05f;
        Gizmos.DrawWireCube(transform.position - new Vector3(0,transform.localScale.y/2 + boxOffset),
                    new Vector2(_collider.size.x,boxOffset) * .5f);
    }

    void GroundCheck(){
        bool wasOnAir = _playerState == PlayerState.Air;
        float boxOffset = .05f;
        var hit = Physics2D.OverlapBox(transform.position - new Vector3(0,transform.localScale.y/2 + boxOffset),
                    new Vector2(_collider.size.x,boxOffset) * .5f,0,
                    LayerMask.GetMask("Floor") | LayerMask.GetMask("Boundary"));
        _playerState = hit != null ? PlayerState.Ground : PlayerState.Air;
        if(wasOnAir && _playerState == PlayerState.Ground){
            AudioManager.instance.PlaySound(GoundSlamSfx);
            ParticleManager.instance.PlayGroundSlamParticle(gameObject);
        }
        if(_playerState == PlayerState.Ground){
            _usedDoubleJump = false;
            _airDashed = false;
        }
    }

    void WallCheck(){
        _touchingWall = Physics2D.Raycast(WallCheckObj.position,Vector2.right * transform.localScale.x,
                    .1f,LayerMask.GetMask("Floor"));
        _canWallJump = _touchingWall && _spd != 0 && _playerState == PlayerState.Air;
        _isWallSliding = _rb2d.velocity.y < 0 && _canWallJump;
        if(_isWallSliding){
            if(!WallSlideParticle.isPlaying)
                WallSlideParticle.Play();
        } else
            WallSlideParticle.Stop();
    }

    public void Move(float val, float moveSpd){
        float v = OverrideValues ? MoveSpeed : moveSpd;
        _spd = val * v;
    }

    public void Jump(float jumpForce){
        switch(_playerState){
            case PlayerState.Ground:
                if(_dashDuration > 0){
                    _anims.ChangeState("Jump");
                    _rb2d.gravityScale = 1;
                    _inputDowntime += PlayerController.DashDuration/2;
                    _dashDuration += PlayerController.DashDuration/2;
                } else
                    _rb2d.velocity = Vector2.zero;
                _rb2d.AddForce(new Vector2(0,jumpForce),ForceMode2D.Impulse);
                AudioManager.instance.PlaySound(sfxID.jump);
                ParticleManager.instance.PlayGroundDustParticle(gameObject);
                break;
            case PlayerState.Air:
                if(_canWallJump)
                    WallJump();
                else
                    DoubleJump(jumpForce);
                break;
        }
    }

    void DoubleJump(float jumpForce){
        if(!_usedDoubleJump && (specialMove & SpecialMove.DoubleJump) != 0) {
            _anims.ChangeState("DoubleJump");
            if (_dashDuration > 0){
                _rb2d.gravityScale = 1;
               _inputDowntime += PlayerController.DashDuration/2;
               _dashDuration += PlayerController.DashDuration/2;
            }
            _usedDoubleJump = true;
            _rb2d.velocity = new Vector2(_rb2d.velocity.x,0);
            _rb2d.AddForce(new Vector2(0,jumpForce),ForceMode2D.Impulse);
            AudioManager.instance.PlaySound(sfxID.jump);
            ParticleManager.instance.PlayDoubleJumpParticle(gameObject);
        }
    }

    void WallJump(){
        _anims.ChangeState("Jump");
        AudioManager.instance.PlaySound(sfxID.jump);
        _airDashed = false;
        _usedDoubleJump = false;
        _inputDowntime = .3f;
        _rb2d.velocity = Vector2.zero;
        _rb2d.AddForce(new Vector2(-transform.localScale.x,1) * PlayerController.WallJumpForce,
                    ForceMode2D.Impulse);
    }

    public void Interact(){
        if(_playerState != PlayerState.Ground || _inputDowntime > 0)
            return;
        if(_interactable != null){
            _interactable.Interact();
            _anims.ChangeState("Interact");
            AudioManager.instance.PlaySound(sfxID.interactions);
            _inputDowntime = .35f;
            _rb2d.velocity = Vector2.zero;
        }else
            Debug.Log("Nothing to interact with");
    }

    public void Special(){
        if((specialMove & SpecialMove.Dash) != 0){
            Dash();
        }
    }

    void Dash(){
        if(_dashDuration > 0 || (_playerState == PlayerState.Air && _airDashed))
            return;
        if(_playerState == PlayerState.Air && !_airDashed)
            _airDashed = true;
        _anims.ChangeState("Dash");
        AudioManager.instance.PlaySound(sfxID.dash);
        _inputDowntime = _dashDuration = PlayerController.DashDuration;
        _rb2d.gravityScale = 0;
        _rb2d.velocity = new Vector2(PlayerController.DashSpeed * transform.localScale.x,0);
        StartCoroutine(DashTimer());
    }

    public void ForceStop(){
        _spd = 0;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        _interactable = other.GetComponent<IInteractable>();
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.GetComponent<IInteractable>() == _interactable)
            _interactable = null;
    }

    IEnumerator DashTimer(){
        float particleTimer = DashGhosts;
        while(_dashDuration > 0){
            yield return new WaitForEndOfFrame();
            _dashDuration -= Time.deltaTime;
            particleTimer -= Time.deltaTime;
            if(particleTimer <= 0){
                particleTimer = DashGhosts;
                ParticleManager.instance.SpriteParticle(gameObject,
                GetComponent<SpriteRenderer>().sprite);
            }
        }
        _rb2d.gravityScale = 1;
    }
}
