using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Trigger2DEvent : MonoBehaviour {
    
    public Collider2DEvent CallTrigger2DEnter;
    public Collider2DEvent CallTrigger2DStay;
    public Collider2DEvent CallTrigger2DExit;

    private void OnTriggerEnter2D(Collider2D other) => CallTrigger2DEnter?.Invoke(other);

    private void OnTriggerStay2D(Collider2D other) => CallTrigger2DStay?.Invoke(other);

    private void OnTriggerExit2D(Collider2D other) => CallTrigger2DExit?.Invoke(other);

}
[System.Serializable]
public class Collider2DEvent : UnityEngine.Events.UnityEvent<Collider2D>{}
[System.Serializable]
public class BoolEvent : UnityEngine.Events.UnityEvent<bool>{}