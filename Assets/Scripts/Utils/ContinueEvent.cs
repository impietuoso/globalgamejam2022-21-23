using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ContinueEvent : MonoBehaviour {
    
    [SerializeField]UnityEngine.Events.UnityEvent OnEventFired;
    bool _fired;

    void Update(){
        if(_fired)
            return;
        if(Gamepad.current != null && Gamepad.current.buttonSouth.wasPressedThisFrame){
            _fired = true;
            OnEventFired?.Invoke();
        }
        if(Keyboard.current.anyKey.wasPressedThisFrame) {
            _fired = true;
            OnEventFired?.Invoke();
        }
    }

}
