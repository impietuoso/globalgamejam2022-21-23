using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowThings : MonoBehaviour, IInteractable
{
    
    [SerializeField]
    private GameObject[] obj;
    [SerializeField]
    private GameObject[] luz;
    private void Start()
    {
        foreach (var item in luz)
        {
            item.SetActive(false);
        }
        
    }
    public void Interact()
    {
        foreach (var item in luz)
        {
            item.SetActive(true);
        }
        foreach (var item in obj)
        {
            item.GetComponent<UnableBlock>().Interagivel();
        }
    }


    
}
