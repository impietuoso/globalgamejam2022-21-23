using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectElevator : MonoBehaviour
{
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            if (collision.transform.position.y - collision.transform.localScale.y / 3 > transform.position.y + transform.localScale.y / 2) 
                GetComponentInParent<MoveElevator>().StartMove();

        
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            if (collision.transform.position.y - collision.transform.localScale.y / 3 > transform.position.y + transform.localScale.y / 2) 
                GetComponentInParent<MoveElevator>().ExitCounter();
        
    }
}
