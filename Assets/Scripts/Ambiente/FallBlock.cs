using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FallBlock : MonoBehaviour
{

    public bool OnlyFromAbove;
    private Rigidbody2D rb;
    public Vector3 spawnPos;
    bool _iHaveFallenAndICantGetUp;
    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        spawnPos = transform.position;
    }
    private void OnEnable()
    {
        rb.bodyType = RigidbodyType2D.Kinematic;
    }

    void Update()
    {
        //if (Keyboard.current.pKey.wasPressedThisFrame) Fall();
    }


    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")){
            //print("called");
            if(OnlyFromAbove){
                if(collision.transform.position.y - collision.transform.localScale.y/3 > transform.position.y + transform.localScale.y/2)
                    Fall();
            } else
                Fall();
        } 
    }
    
    
    private void Fall() {
        if(_iHaveFallenAndICantGetUp)
            return;
        
        _iHaveFallenAndICantGetUp = true;
        AudioManager.instance.StartCoroutine(WaitFall());
    }
   
    private IEnumerator WaitFall()
    {
        yield return new WaitForSeconds(0.35f);
        rb.bodyType = RigidbodyType2D.Dynamic;
        yield return new WaitForSeconds(0.8f);
        gameObject.SetActive(false);
        yield return new WaitForSeconds(5f);
        transform.position = spawnPos;
        gameObject.SetActive(true);
        _iHaveFallenAndICantGetUp = false;
    }
    

}

