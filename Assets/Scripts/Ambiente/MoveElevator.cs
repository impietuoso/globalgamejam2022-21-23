using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveElevator : MonoBehaviour {
    [SerializeField]
    private GameObject obj;
    
    public int cont = 0;

    [SerializeField]
    private Transform goalPos;
    private bool canMove = false;
    public bool done;

    public bool endGame;

    bool _gameHasEnded;
    public void StartMove() {
        cont++;
        if (cont == 2) {
            //mover
            canMove = true;
            PlayerController.instance.enabled = false;
            //camera
        }
    }
    public void ExitCounter() {
        cont--;
    }
    private void Update()
    {
        if(_gameHasEnded)
            return;
        if (canMove) {
            if (Vector3.Distance(obj.transform.position, goalPos.position) >= 0.01f) {
                obj.transform.position = Vector3.MoveTowards(obj.transform.position, goalPos.position, 0.01f);
            } else {
                if(endGame){
                    EndGameBandAid.instanace.ShowMessage();
                    _gameHasEnded = true;
                } else
                    PlayerController.instance.enabled = true;
                if (!done) {
                    MapaIndicator.instance.ActiveMap();
                    done = true;
                }
            }
        }
    }
}
