using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UnableBlock : MonoBehaviour
{
    private Collider2D col;

    private SpriteRenderer rend;

    private Color cor = Color.white;
    void Start()
    {
        col = gameObject.GetComponent<Collider2D>();
        rend = gameObject.GetComponent<SpriteRenderer>();
        cor.a = 0.3f;
        col.enabled = false;
    }

    
    void Update()
    {
        //if (Keyboard.current.pKey.wasPressedThisFrame) Interagivel();

        rend.color = cor;
    }

    public void Interagivel() {
        col.enabled = !col.enabled;

        if (cor.a == 1) cor.a = 0.3f;
        else cor.a = 1;

        
    }
}
