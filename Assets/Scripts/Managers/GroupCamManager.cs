using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GroupCamManager : MonoBehaviour {
    
    CinemachineTargetGroup _targetGroup;

    void Awake() => _targetGroup = GetComponent<CinemachineTargetGroup>();
    
    public void ToggleWeight(){
        for(int i = 0;i < _targetGroup.m_Targets.Length;i++)
            _targetGroup.m_Targets[i].weight = Mathf.Abs( _targetGroup.m_Targets[i].weight - 1);
    }
}
