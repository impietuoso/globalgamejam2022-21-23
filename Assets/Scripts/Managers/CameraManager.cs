using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
    
    [SerializeField]GameObject[] Cameras;

    public void ActivateCamera(int index){
        for(int i = 0;i < Cameras.Length;i++)
            Cameras[i].SetActive(i == index);
    }

}
