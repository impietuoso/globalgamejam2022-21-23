using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    float[] _timer = new float[5];
    int _timerIndex;

    bool _runTimer;

    [SerializeField]ScoreUIController ScoreController;

    //[Header("Game Over Variables")]
    [SerializeField]UnityEngine.Events.UnityEvent OnEndCutsceneEnd,OnGameOverEnd;
    [SerializeField]GameObject GameOverMessage;
    [SerializeField]float GameOverFadeTime,GameOverStandByTime;
    [SerializeField]PlayerMovement[] Characters;
    [SerializeField]float EndEventDelay,EndEventStandBy;
    [SerializeField]CanvasGroup ScoreGroup;


    CanvasGroup _group;

    public void EndCutscene() => StartCoroutine(WalkTowaradsCenter());

    public void ShowMessage(){
        if(_group == null)
            _group = GameOverMessage.GetComponent<CanvasGroup>();
        LeanTween.value(GameOverMessage,(val)=>_group.alpha = val,0,1,GameOverFadeTime).setOnComplete(ShowScore);                        
    }

    void ShowScore(){
        ScoreController.UpdateScore(_timer);
        LeanTween.value(GameOverMessage,(val)=>ScoreGroup.alpha = val,
                        0,1,GameOverFadeTime).setOnComplete(()=>StartCoroutine(GetOutTimer())).setDelay(GameOverStandByTime);
    }

    public void BeginTimer(int index){
        _timerIndex = index;
        _runTimer = true;
    }

    public void StopTimer(){
        _runTimer = false;
    }

    void Update(){
        if(_runTimer)
            _timer[_timerIndex] += Time.deltaTime;
    }

    IEnumerator GetOutTimer(){
        yield return new WaitForSecondsRealtime(GameOverStandByTime);
        OnGameOverEnd?.Invoke();
    }

    IEnumerator WalkTowaradsCenter(){
        yield return new WaitForSeconds(EndEventDelay);
        Characters[0].Move(Characters[0].transform.localPosition.x > 0 ? -1 : 1,1);
        Characters[1].Move(Characters[1].transform.localPosition.x > 0 ? -1 : 1,1);
        yield return new WaitUntil(
            ()=> Vector2.Distance(Characters[0].transform.position,Characters[1].transform.position) < 1);
        Characters[0].Move(0,1);
        Characters[1].Move(0,1);
        yield return new WaitForSeconds(EndEventStandBy);
        OnEndCutsceneEnd?.Invoke();
    }
}
