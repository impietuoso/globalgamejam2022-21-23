using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OptionMenuUIController : MonoBehaviour {
    
    [SerializeField]GameObject FirstSelected;

    public void Enable() {
        StartCoroutine(DelayForInput(FirstSelected));
    }

    public void Disable(GameObject nextFirstSelected) {
        StartCoroutine(DelayForInput(nextFirstSelected));
    }

    IEnumerator DelayForInput(GameObject toActivate) {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(toActivate);
    }

}
