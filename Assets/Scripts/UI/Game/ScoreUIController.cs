using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreUIController : MonoBehaviour {
    
    [SerializeField]TMPro.TextMeshProUGUI[] ScoreTextVals;

    public void UpdateScore(float[] vals) {
        float total = 0;
        for(int i = 0; i < vals.Length; i++) {
            TimeSpan timeSpan = TimeSpan.FromSeconds(vals[i]);
            string timeText = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
            ScoreTextVals[i].text = timeText;
            total += vals[i];
        }
        TimeSpan ts = TimeSpan.FromSeconds(total);
        string tt = string.Format("{0:D2}:{1:D2}", ts.Minutes, ts.Seconds);
        ScoreTextVals[ScoreTextVals.Length - 1].text = tt;
        if(PlayerPrefs.GetFloat("time_total", 0) > 0){
            if(PlayerPrefs.GetFloat("time_total", 0) > total)
                SaveProgress(vals, total);
        } else
            SaveProgress(vals, total);
    }

    void SaveProgress(float[] time, float total) {
        for (int i = 0; i < time.Length; i++) {
            PlayerPrefs.SetFloat($"time_{i}", time[i]);
        }
        PlayerPrefs.SetFloat("time_total", total);
    }

}
