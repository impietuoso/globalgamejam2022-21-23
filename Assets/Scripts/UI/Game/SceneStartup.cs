using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneStartup : MonoBehaviour
{
    public Animator transition;
    private void Start() {
        transition.Play("FadeOut");
    }
}
