using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class MenuUIController : MonoBehaviour {
    
    //public GameObject CurrentMenu;
    Button CancelButton;

    GameInputs _inputs;

    [SerializeField] TMPro.TextMeshProUGUI[] ScoreTextVals;

    [SerializeField] GameObject recordButton;

    void Awake() {
        SetInputCallbacks();
        if (recordButton != null)
            recordButton.SetActive(PlayerPrefs.GetFloat("time_total", 0f) > 0);
        //print(PlayerPrefs.GetFloat("time_total", 0f)>0);
    }

    private void OnEnable() => _inputs.Enable();

    private void OnDisable() => _inputs.Disable();

    void SetInputCallbacks(){
        _inputs = new GameInputs();
        _inputs.UI.Cancel.performed += (ctx) => Cancel();
    }

    void Cancel(){
        if(CancelButton != null)
            CancelButton.onClick.Invoke();
    }

    public void SetCancelButton(Button c) {
        if (c != null)
            CancelButton = c;
    }

    public void ClearCancelButton() {
        if (CancelButton != null)
            CancelButton = null;
    }

    public void SetNextFirstSelected(GameObject nextFirstSelected) {
        StartCoroutine(DelayForInput(nextFirstSelected));
    }

    IEnumerator DelayForInput(GameObject toActivate) {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(toActivate);
    }

    public void LoadRecord() {
        for (int i = 0; i < ScoreTextVals.Length; i++) {
            TimeSpan timeSpan = TimeSpan.FromSeconds(PlayerPrefs.GetFloat($"time_{i}", 0f));
            string timeText = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
            ScoreTextVals[i].text = timeText;
        }
        TimeSpan ts = TimeSpan.FromSeconds(PlayerPrefs.GetFloat("time_total", 0f));
        string tt = string.Format("{0:D2}:{1:D2}", ts.Minutes, ts.Seconds);
        ScoreTextVals[ScoreTextVals.Length - 1].text = tt;
    }


}
