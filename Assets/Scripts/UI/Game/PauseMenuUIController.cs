using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PauseMenuUIController : MonoBehaviour {
    
    [SerializeField]GameObject FirstSelected;

    [SerializeField]UnityEngine.Events.UnityEvent OnPauseEnable;

    void OnEnable() {
        StartCoroutine(DelayForInput(FirstSelected));
        OnPauseEnable?.Invoke();
    }

    private void OnDisable() {
        EventSystem.current.SetSelectedGameObject(null);
    }

    IEnumerator DelayForInput(GameObject toActivate) {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(toActivate);
    }

}
