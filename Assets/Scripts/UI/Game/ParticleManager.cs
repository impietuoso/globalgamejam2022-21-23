using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {
    
    public static ParticleManager instance {get;protected set;}

    [SerializeField]GameObject ActiveParticle,DeactiveParticle,GroundDustParticle,GroundSlamParticle,DoubleJumpParticle;
    [SerializeField]GameObject SpriteParticlePrefab;

    private void Awake() {
        instance = this;
    }

    public void PlayActiveParticle(GameObject target) => Instantiate(ActiveParticle,target.transform.position,Quaternion.identity,transform);
    public void PlayDeactiveParticle(GameObject target) => Instantiate(DeactiveParticle,target.transform.position,Quaternion.identity,transform);
    public void PlayGroundSlamParticle(GameObject target) => Instantiate(GroundSlamParticle,target.transform.position,Quaternion.identity,transform);
    public void PlayGroundDustParticle(GameObject target){
        GameObject obj = Instantiate(GroundDustParticle,target.transform.position,Quaternion.identity,transform);
        Destroy(obj,3);
    }

    public void PlayDoubleJumpParticle(GameObject target){
        if(DoubleJumpParticle == null)
            return;
        GameObject p = Instantiate(DoubleJumpParticle,target.transform.position,Quaternion.identity,transform);
        var ps = p.GetComponent<ParticleSystem>();
        if(ps != null){
            var main = ps.main;
            main.loop = false;
            main.stopAction = ParticleSystemStopAction.Destroy;
            return;
        }
        Destroy(p,3);
    }

    public void SpriteParticle(GameObject target,Sprite clone){
        GameObject obj = Instantiate(SpriteParticlePrefab,target.transform.position,Quaternion.identity,transform);
        obj.transform.localScale = target.transform.localScale;
        SpriteRenderer r = obj.GetComponent<SpriteRenderer>();
        r.sprite = clone;
        r.enabled = true;
        LeanTween.value(gameObject,(val)=> r.color = val,r.color,
                        new Color(1,1,1,0),.5f).setOnComplete(()=>Destroy(obj));
    }
    
}
