using UnityEngine;
using UnityEngine.UI;

public class BackgroundColorChange : MonoBehaviour {
    
    enum DayNight {Day,Night}

    [SerializeField]Color DayColor,NightColor;
    [SerializeField]DayNight StartingColor;
    byte _colorIndex;
    Image _BGImage;
    void Awake(){
        _BGImage = GetComponent<Image>();
        switch(StartingColor){
            case DayNight.Day:
                _colorIndex = 0;
                _BGImage.color = DayColor;
                break;
            case DayNight.Night:
                _colorIndex = 1;
                _BGImage.color = NightColor;
                break;
        }
    }

    public void ToggleColor(){
        _colorIndex++;
        LeanTween.value(gameObject,(c)=> _BGImage.color = c,
                        _BGImage.color,_colorIndex%2 == 0 ? DayColor: NightColor,1f);
        //_BGImage.color = _colorIndex%2 == 0 ? DayColor: NightColor;
    }
}
