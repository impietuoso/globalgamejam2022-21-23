using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitions : MonoBehaviour
{
    public void GoToScene(int scene) {
        StartCoroutine(Transitionscene(scene));
    }

    public void ExitGame() {
        Application.Quit();
    }

    IEnumerator Transitionscene(int scene) {
        GetComponent<Animator>().Play("FadeIn");
        AudioManager.instance.FadeAudio();
        yield return new WaitForSeconds(3f);
        SceneManager.LoadSceneAsync(scene);
    }
}
