using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    #region Variables
    public static AudioManager instance;

    [Header("AudioSources")]
    public AudioSource musicAS; //M�sica
    public AudioSource soundAS; //Sound Effects

    [Header("AudiosClips")]
    public AudioClip[] music; //Lista de M�sicas
    public AudioClip[] soundEffect; //Lista de Sound Effects

    bool musicMute; //Mute de M�sicas
    bool soundMute; //Mute dos Sound Effects
    float currentMusicVolume; //Volume das m�sicas
    float currentSoundVolume; //Volume dos Sound Effects

    [Header("UI Components")]
    public Slider musicVolumeSlider;
    public Slider soundVolumeSlider;
    public Toggle musicToggle;
    public Toggle soundToggle;
    #endregion

    #region Setup GameObject
    private void Awake() {
        instance = this; //Criando Singleton
    }

    private void Start() {
        SetupAudioUI(); //Ajustar volume da m�sica e dos sound effects salvos
        musicAS.volume = 0;
        soundAS.volume = 0;
        StartCoroutine(VolumeFader(true));
    }

    public void SetupAudioUI() {
        LoadVolumes();
        GetCurrentMusicVolume();
        GetCurrentSoundVolume();
    }

    #endregion

    #region Play Audio
    public void PlayMusica(int musicCode) {
        musicAS.clip = music[musicCode]; //Muda clip para tocar
        musicAS.Play(); //Toca novo clip
    }

    public void PlaySound(sfxID soundCode) {
        soundAS.PlayOneShot(soundEffect[(int)soundCode]); //Toca sound effect imediatamente
    }

    public void PlaySound(int soundCode) {
        soundAS.PlayOneShot(soundEffect[soundCode]); //Toca sound effect imediatamente
    }
    #endregion

    #region Music
    public void SetCurrentMusicVolume() {
        currentMusicVolume = musicVolumeSlider.value/20; //Pega o valor do novo volume
        musicAS.volume = currentMusicVolume; //Muda o volume atual para o novo volume
        if (currentMusicVolume == 0 && musicAS.mute == false) {
            musicAS.mute = true;
            musicToggle.isOn = true;
            musicMute = true;
        } else if(currentMusicVolume != 0 && musicAS.mute == true) {
            musicAS.mute = false;
            musicToggle.isOn = false;
            musicMute = false;
        }

        PlayerPrefs.SetFloat("currentMusicVolume", currentMusicVolume); //Passa para o Player Preferences o valor atual do volume da musica
    }

    public void GetCurrentMusicVolume() {
        musicVolumeSlider.value = currentMusicVolume*20; //Pega o slider de volume e muda o valor para o volume atual da musica
        if (currentMusicVolume < 0) {
            musicAS.mute = true;
            musicToggle.isOn = true;
            musicMute = true;
        } else {
            musicAS.mute = false;
            musicToggle.isOn = false;
            musicMute = false;
        }
    }
    #endregion

    #region Sound Effect
    public void SetCurrentSoundVolume() {
        currentSoundVolume = soundVolumeSlider.value / 20; //Pega o valor do novo volume
        soundAS.volume = currentSoundVolume; //Muda o volume atual para o novo volume
        if (currentSoundVolume == 0 && soundAS.mute == false) {
            soundAS.mute = true;
            soundToggle.isOn = true;
            soundMute = true;
        } else if (currentSoundVolume != 0 && soundAS.mute == true) {
            soundAS.mute = false;
            soundToggle.isOn = false;
            soundMute = false;
        }

        PlayerPrefs.SetFloat("currentSoundVolume", currentSoundVolume); //Passa para o Player Preferences o valor atual do volume do sound effet
    }

    public void GetCurrentSoundVolume() {
        soundVolumeSlider.value = currentSoundVolume*20; //Pega o slider de volume e muda o valor para o volume atual do sound effect
        if (currentSoundVolume < 0) {
            soundAS.mute = true;
            soundToggle.isOn = true;
            soundMute = true;
        } else {
            soundAS.mute = false;
            soundToggle.isOn = false;
            soundMute = false;
        }
    }
    #endregion

    #region Music Mute
    public void SetMusicMute() {
        musicMute = musicToggle.isOn;
        musicAS.mute = musicToggle.isOn;
        musicAS.volume = currentMusicVolume; //Muda o volume atual para o novo volume
    }
    #endregion

    #region Sound Effect Mute
    public void SetSoundMute() {
        soundMute = soundToggle.isOn;
        soundAS.mute = soundToggle.isOn;
        soundAS.volume = currentSoundVolume; //Muda o volume atual para o novo volume
    }
    #endregion    

    #region Load
    void LoadVolumes() {
        currentMusicVolume = PlayerPrefs.GetFloat("currentMusicVolume", 0.2f); //Pega o volume da musica salvo no player preferences
        currentSoundVolume = PlayerPrefs.GetFloat("currentSoundVolume", 0.3f); //Pega o volume do sound effect salvo no player preferences
        musicAS.volume = currentMusicVolume; //Muda o volume atual para o volume da musica salvo no player preferences
        soundAS.volume = currentSoundVolume; //Muda o volume atual para o volume do sound effect salvo no player preferences
    }
    #endregion

    #region Fade Audio
    public void FadeAudio() {
        StartCoroutine(VolumeFader(false));
    }

    IEnumerator VolumeFader(bool grow) {
        float value1 = grow ? currentMusicVolume : 0;
        float value2 = grow ? currentSoundVolume : 0;
        float lerpTime = 0;
        while (lerpTime < 1) {
            lerpTime += Time.deltaTime;
            musicAS.volume = Mathf.Lerp(musicAS.volume, value1, lerpTime);
            soundAS.volume = Mathf.Lerp(soundAS.volume, value2, lerpTime);
            yield return null;
        }
    }
    #endregion

}


public enum sfxID {
    changeCharacters,
    dash,
    footstep,
    fall,
    jump,
    pause,
    play,
    unpause,
    wallslide,
    interactions
}
