using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SimpleUIMenuController : MonoBehaviour {
    
[SerializeField]GameObject FirstSelected;

    public void OnEnable() {
        StartCoroutine(DelayForInput(FirstSelected));
    }

    public void Disable(GameObject nextFirstSelected) {
        StartCoroutine(DelayForInput(nextFirstSelected));
    }

    IEnumerator DelayForInput(GameObject toActivate) {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(toActivate);
    }

    
}
