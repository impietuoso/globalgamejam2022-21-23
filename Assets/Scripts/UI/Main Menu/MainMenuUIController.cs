using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenuUIController : MonoBehaviour {
    
    [SerializeField]GameObject FirstSelected;

    public void Enable() {
        StartCoroutine(DelayForInput(FirstSelected));
    }

    IEnumerator DelayForInput(GameObject toActivate) {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(toActivate);
    }


}
