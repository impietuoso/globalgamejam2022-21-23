using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftController : MonoBehaviour {
    
    [SerializeField]UnityEngine.Events.UnityEvent OnLiftStart,OnLiftEnd,OnLiftStop;
    [SerializeField]float LiftTime,LiftDelay,LiftStandBy;

    LiftDetector[] _detectors;

    bool _fired;

    void Awake(){ 
        _detectors = GetComponentsInChildren<LiftDetector>();
        for(int i = 0;i < _detectors.Length; i++){
            _detectors[i].OnTargetEnter += ConfirmDetection;
            _detectors[i].OnMoveFinished += ConfirmFinished;
        }
    }

    void ConfirmDetection(){
        for(int i = 0;i < _detectors.Length; i++){
            if(!_detectors[i].ConfirmDetection())
                return;
        }
        PlayerController.instance.enabled = false;
        for(int i = 0;i < _detectors.Length; i++)
            _detectors[i].MoveTargetToCenter();
    }

    void ConfirmFinished(){
        for(int i = 0;i < _detectors.Length; i++){
            if(!_detectors[i].ConfirmFinished())
                return;
        }
        StartLift();
    }

    void StartLift(){
        OnLiftStart?.Invoke();
        for(int i = 0;i < _detectors.Length; i++)
            LeanTween.moveLocalY(_detectors[i].transform.parent.gameObject,0,LiftTime).setDelay(LiftDelay).setOnComplete(
                () => OnLiftStop?.Invoke()
            );
        StartCoroutine(EndLift());
    }

    IEnumerator EndLift(){
        yield return new WaitForSeconds(LiftTime + LiftStandBy);
        if(!_fired){
            OnLiftEnd?.Invoke();
            foreach(LiftDetector l in _detectors)
                Destroy(l);
            Destroy(this);
        } else
            print("Called twice");
        _fired = true;
        //PlayerController.instance.enabled = true;
    }
}
