using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftDetector : MonoBehaviour {
    
    [SerializeField]GameObject TriggerDetection;
    public event System.Action OnTargetEnter,OnMoveFinished;
    PlayerMovement _target;
    bool _finished;

    bool AtAceptableDistance {
        get{
            if(_target == null)
                return false;
            return Vector2.Distance(_target.transform.position,
         new Vector2(transform.position.x,_target.transform.position.y)) < .1f;
        }
    } 

    public void TriggerEnter(Collider2D col){
        if(_finished)
            return;
        _target = col.GetComponent<PlayerMovement>();
        if(_target != null)
            OnTargetEnter?.Invoke();
    }

    public void TriggerExit(Collider2D col){
        var t = col.GetComponent<PlayerMovement>();
        if(_target == t)
            _target = null;
    }

    public void MoveTargetToCenter(){
        if(_target != null)
            StartCoroutine(StartMoving());
    }

    public bool ConfirmFinished() => AtAceptableDistance;

    public bool ConfirmDetection(){
        var hit = Physics2D.OverlapBox(transform.position + new Vector3(0, 1.5f),
                                        new Vector2(1.75f,.9f),0,LayerMask.GetMask("Player"));
        return hit != null;
    }

    IEnumerator StartMoving(){
        _target.Move(_target.transform.position.x > transform.position.x ? -1 : 1,3);
        yield return new WaitUntil(() => AtAceptableDistance);
        _target.transform.localScale = new Vector2(transform.parent.localPosition.x > 0 ? -1 : 1,1);
        _target.Move(0,0);
        OnMoveFinished?.Invoke();
        //gameObject.SetActive(false);
    }
}
